package ch.bbw.vererbungVoegel;

/**
 * @author Moritz Bolliger
 * @version 27.03.2019
 * @project Package ch.bbw.vererbungVoegel von Projekt VererbungVögel
 * @day Mittwoch
 * @time 15:05
 */
public class Vogel {

    protected String name, farbe;

    public Vogel(String name, String farbe) {
        this.name = name;
        this.farbe = farbe;
    }

    public void fliegen(){
        System.out.println("Vogel " + name + " Fliegt");
    }

    public void flattern(){
        System.out.println("Vogel " + name + " flattert");
    }
}
