package ch.bbw.vererbungVoegel;

/**
 * @author Moritz Bolliger
 * @version 27.03.2019
 * @project Package ch.bbw.vererbungVoegel von Projekt VererbungVögel
 * @day Mittwoch
 * @time 15:06
 */
public class Huhn extends Vogel{

    public Huhn(String name, String farbe) {
        super(name, farbe);
    }

    @Override
    public void fliegen(){
        System.out.println("Huhn " + name + " fliegt");
    }

    public void superFlattern(){
        super.flattern();
    }
}
