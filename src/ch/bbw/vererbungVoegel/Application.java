package ch.bbw.vererbungVoegel;

/**
 * @author Moritz Bolliger
 * @version 27.03.2019
 * @project Package ch.bbw.vererbungVoegel von Projekt VererbungVögel
 * @day Mittwoch
 * @time 15:03
 */
public class Application {

    public static void main(String[] args) {

        Vogel birdy = new Vogel("Birdy", "blau");
        Huhn helena = new Huhn("helena", "pink");
        Ente quak = new Ente("quak", "schwarz");
        quak.schwimmen();
        helena.fliegen();
        helena.flattern();
        birdy.fliegen();
        birdy.flattern();

        Vogel einVogel = birdy;
        einVogel.fliegen();
        Vogel nocheinVogel = helena;
        nocheinVogel.fliegen();

        Ente emma = quak;
        emma.schwimmen();

        Vogel beate = new Ente("beate", "gefleckt");
        beate.fliegen();
        //beate.schwimmen();
        //((Ente) birdy).schwimmen();
        ((Ente) beate).schwimmen();

        helena.superFlattern();
    }
}
