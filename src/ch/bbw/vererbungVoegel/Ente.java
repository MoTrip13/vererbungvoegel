package ch.bbw.vererbungVoegel;

/**
 * @author Moritz Bolliger
 * @version 27.03.2019
 * @project Package ch.bbw.vererbungVoegel von Projekt VererbungVögel
 * @day Mittwoch
 * @time 15:06
 */
public class Ente extends Vogel{

    public Ente(String name, String farbe) {
        super(name, farbe);
    }

    public void schwimmen(){
        System.out.println("Ente " + name + " schwimmt");
        int a = 3+3;
    }
}
